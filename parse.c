#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "parse.h"

#define DELIMITERS " \t\v\f\r\0\n"
#define MAX_TOKENS 40

bool debug_mode_on = false;

static char* str_duplicate(const char* str)
{
	int n = strlen(str) + 1;
	char *dup = malloc(n);
	if (dup)
	{
		strcpy(dup, str);
	}
	return dup;
}

static int is_lowercase_string(char* s)
{
	int len = strlen(s);
	int i = 0;
	while (i < len)
	{
		if (!islower(s[i])) return 0;
		i++;
	}
	return 1;
}

static int is_valid_number(char* s)
{
	if (strlen(s) > 1 && s[0] == '0')
	{                   //only possible number 
		return 0;       //starting with 0 is "0".
	}
	else
	{
		for (int i = 0; s[i] != '\0'; i++)
			if (!isdigit(s[i])) return 0;
	}
	return 1;
}


int split_line(char* line, char*** argv)
{
	char* buffer = str_duplicate(line);
	char* tmp = strrchr(buffer, '\n');      //replace newline from the end of the line
	if (tmp) *tmp = '\0';                   //with null character
	int argc = 0;
	(*argv)[argc++] = str_duplicate(strtok(buffer, DELIMITERS));
	while (((tmp = strtok(NULL, DELIMITERS)) != NULL) && (argc < MAX_TOKENS))
		(*argv)[argc++] = str_duplicate(tmp);

    free(buffer);
    printf("po free buffer\n");
	return argc;
}


int process_command(char* line, char*** args)
{
	char** tokens = malloc(sizeof(char*) * MAX_TOKENS);
    printf("przed split_line\n");
	int arg_count = split_line(line, &tokens);
    printf("po split_line\n");
	int exit_code;

	if (arg_count == 0)
	{
		exit_code = -1;
	}
	else
	{
		if (tokens[0] == NULL)
		{
			exit_code = -1;
		}
		else if (!strcmp(tokens[0], "insert"))
		{
			if (arg_count != 2)
				exit_code = -1;
			else
				if (!is_lowercase_string(tokens[1]))
				{
					exit_code = -1;
				}
				else
				{
					(*args)[0] = str_duplicate(tokens[1]);
					exit_code = 1;
				}
		}
		else if (!strcmp(tokens[0], "prev"))
		{
			if (arg_count != 4)
				exit_code = -1;
			else
				if (is_valid_number(tokens[1]) &&
					is_valid_number(tokens[2]) &&
					is_valid_number(tokens[3]))
				{
					if (atol(tokens[2]) <= atol(tokens[3]))
					{
						(*args)[0] = str_duplicate(tokens[1]);
						(*args)[1] = str_duplicate(tokens[2]);
						(*args)[2] = str_duplicate(tokens[3]);
						exit_code = 2;
					}
					else
					{
						exit_code = -1;
					}
				}
				else
				{
					exit_code = -1;
				}
		}
		else if (!strcmp(tokens[0], "delete"))
		{
			if (arg_count != 2)
				exit_code = -1;
			else
				if (!is_valid_number(tokens[1]))
				{
					exit_code = -1;
				}
				else
				{
					(*args)[0] = str_duplicate(tokens[1]);
					exit_code = 3;
				}
		}
		else if (!strcmp(tokens[0], "find"))
		{
			if (arg_count != 2)
				exit_code = -1;
			else
				if (!is_lowercase_string(tokens[1]))
				{
					exit_code = -1;
				}
				else
				{
					(*args)[0] = str_duplicate(tokens[1]);
					exit_code = 4;
				}
		}
		else if (!strcmp(tokens[0], "clear"))
		{
			if (arg_count > 1) exit_code = -1;
			else exit_code = 5;
		}
		else
		{
			exit_code = -1;
		}
	}
    free(tokens);
	return exit_code;
}

bool parse_arguments(int argc, char** argv)
{
	if (argc > 2)
	{
		return false;
	}
	else if (argc == 2)
	{
		if (strcmp(argv[1], "-v"))
		{
			return false;
		}
		else
		{
			debug_mode_on = true;
			return true;
		}
	}
	else
	{
		return true;
	}
}
