CC = gcc
CFLAGS = -Wall -std=c99 -ggdb

NAME = dictionary
NAMED = dictionary.dbg
OBJECTS = parse.o dictionary.o trie.o

.PHONY: all clean debug

all: dictionary

trie.o: trie.h trie.c
	$(CC) $(CFLAGS) -c trie.c

parse.o: parse.h parse.c
	$(CC) $(CFLAGS) -c parse.c

dictionary.o: dictionary.c
	$(CC) $(CFLAGS) -c dictionary.c

dictionary: $(OBJECTS) 
	$(CC) $(CFLAGS) -o $(NAME) $(OBJECTS)
	
debug: clean
debug: CFLAGS += -g
debug: NAME = $(NAMED)
debug: all

clean:
ifneq ("$(wildcard $(NAMED))","")
	rm -f dictionary.dbg
endif
	rm -f $(OBJECTS)
