#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parse.h"
#include "trie.h"

#define MAX_STRING_LENGTH 100000

typedef struct dictionary
{
	Trie* tree;
} dictionary;

extern bool debug_mode_on;

bool dictionary_insert(dictionary* dict, char* word)
{
	return trie_insert(&(dict->tree), word);
}


bool dictionary_prev(dictionary* dict, long num, long start, long end)
{
	return trie_prev(&(dict->tree), num, start, end);
}


bool dictionary_delete(dictionary* dict, long num)
{
	return trie_delete(&(dict->tree), num);
}


bool dictionary_find(dictionary* dict, char* pattern)
{
	return trie_find(dict->tree, pattern);
}


void dictionary_clear(dictionary* dict)
{
	trie_clear(&(dict->tree));
}


void execute_command(dictionary* dict, int command_code, char** argv)
{
	switch (command_code)
	{
	case 1:
        printf("insert %s\n", argv[0]);
		if (dictionary_insert(dict, argv[0]))
		{
			printf("word number: %lu\n", dict->tree->words_amount - 1);
			if (debug_mode_on)
				fprintf(stderr, "nodes: %lu\n", dict->tree->nodes_amount);
		}
		else
		{
			printf("ignored\n");
		}

		break;
	case 2:
		if (dictionary_prev(dict, atol(argv[0]), atol(argv[1]), atol(argv[2])))
		{
			printf("word number: %lu\n", dict->tree->words_amount - 1);
			if (debug_mode_on)
				fprintf(stderr, "nodes: %lu\n", dict->tree->nodes_amount);
		}
		else
		{
			printf("ignored\n");
		}
		break;
	case 3:
		if (dictionary_delete(dict, atol(argv[0])))
		{
			printf("deleted: %lu\n", atol(argv[0]));
			if (debug_mode_on)
				fprintf(stderr, "nodes: %lu\n", dict->tree->nodes_amount);
		}
		else
		{
			printf("ignored\n");
		}
		break;
	case 4:
		if (dictionary_find(dict, argv[0]))
			printf("YES\n");
		else
			printf("NO\n");
		break;
	case 5:
		dictionary_clear(dict);
		printf("cleared\n");
		if (debug_mode_on)
			fprintf(stderr, "nodes: 0\n");
		break;
	default:
		printf("ignored\n");
	}

}


int main(int argc, char** argv)
{
	char* line = malloc(sizeof(char) * (MAX_STRING_LENGTH));
	char** args = malloc(sizeof(char*) * 3);
	memset(args, NULL, 3);
	dictionary d = { .tree = NULL };

	if (!parse_arguments(argc, argv))
	{
		printf("Nieprawidlowe argumenty wywolania. Nastepuje przerwanie wykonania programu.\n");
		return 1;
	}

	while (fgets(line, MAX_STRING_LENGTH, stdin) != NULL)
	{
		int res = process_command(line, &args);
		execute_command(&d, res, args);
		for (int i = 0; i < 3; i++)
			if (args[i] != NULL)
			{
				free(args[i]);
				args[i] = NULL;
			}
	}

	free(line);
	free(args);
	trie_clear(&(d.tree));
	free(d.tree);

	system("pause");

	return 0;
}
