#ifndef TRIE_H   /* Include guard */
#define TRIE_H

#define ALPHABET_SIZE 26
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct t_node
{
	char* label;                    		//Label of the node
	long num;                       		//Number of word represented by the node. -1 if it's not a word.
	long prefix_len;                		//Amount of characters before this node.
	struct t_node* parent;                 	//Pointer to parent of the node.
	struct t_node* children[ALPHABET_SIZE];	//Pointers to children of the node.
} trie_node;

typedef struct a_node
{
	trie_node* key;			//Node from trie
	struct a_node* left;	//Left child
	struct a_node* right;	//Right child
	int height;				//Level of node
} avl_node;

typedef struct Trie
{
	trie_node* root;		//Root of trie
	avl_node* avl_root;     //Root of avl tree of nodes
	long nodes_amount;		//Amount of nodes
	long words_amount;		//Amount of words in trie
} Trie;

bool trie_insert(Trie** t, char* word);

bool trie_find(Trie* t, char* word);

bool trie_prev(Trie** t, long num, long from, long to);

bool trie_delete(Trie** t, long num);

void trie_clear(Trie** t);

#endif
