#ifndef PARSER_H   /* Include guard */
#define PARSER_H

#include <stdbool.h>

int process_command(char* line, char*** args);

bool parse_arguments(int argc, char** argv);

#endif
