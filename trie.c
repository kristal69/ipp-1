#include "trie.h"
#include <stdbool.h>

#define max(a,b) (((a) > (b)) ? (a) : (b))

//Creates a new copy of a given string
static char* str_duplicate(const char* str)
{
	int n = strlen(str) + 1;
	char *dup = malloc(n);
	if (dup)
	{
		strcpy(dup, str);
	}
	return dup;
}


//Returns substring of given string, including begin and end indexes
static char* subString(char* string, long begin, long end)
{
	long n;
	char* new = malloc(sizeof(char)*(end - begin + 2));

	for (n = 0; n <= end - begin; n++)
	{
		new[n] = string[n + begin];
	}
	new[end - begin + 1] = '\0';

	return new;
}


//AVL tree code
//------------------------------

//Returns height of the AVL tree
static int avl_height(avl_node* node)
{
	if (node == NULL)
		return 0;
	return node->height;
}


//A function to create a new avl_node with specified key
static avl_node* avl_new_node(trie_node* key)
{
	avl_node* node = malloc(sizeof(avl_node));
	node->key = key;
	node->left = NULL;
	node->right = NULL;
	node->height = 1;  // new node is initially added at leaf
	return node;
}


//A utility function to right rotate subtree rooted with y
static avl_node* avl_rotate_right(avl_node* y)
{
	avl_node* x = y->left;
	avl_node* z = x->right;

	// Perform rotation
	x->right = y;
	y->left = z;

	// Update heights
	y->height = max(avl_height(y->left), avl_height(y->right)) + 1;
	x->height = max(avl_height(x->left), avl_height(x->right)) + 1;

	// Return new root
	return x;
}


// A utility function to left rotate subtree rooted with x
static avl_node* avl_rotate_left(avl_node* x)
{
	avl_node* y = x->right;
	avl_node* z = y->left;

	// Perform rotation
	y->left = x;
	x->right = z;

	//  Update heights
	x->height = max(avl_height(x->left), avl_height(x->right)) + 1;
	y->height = max(avl_height(y->left), avl_height(y->right)) + 1;

	// Return new root
	return y;
}


//Returns balance factor of current node
static int avl_get_balance(avl_node* n)
{
	if (n == NULL)
		return 0;
	return avl_height(n->left) - avl_height(n->right);
}


//Inserts key into avl tree.
static avl_node* avl_insert(avl_node* node, trie_node* key)
{
	//Perform regular BST insertion
	if (node == NULL)
		return(avl_new_node(key));

	if (key->num < node->key->num)
		node->left = avl_insert(node->left, key);
	else
		node->right = avl_insert(node->right, key);

	//Update height
	node->height = max(avl_height(node->left), avl_height(node->right)) + 1;

	//Get balance factor of the node
	int balance = avl_get_balance(node);

	//If node is imbalanced there are 4 cases:

	//Left left
	if ((balance > 1) && (key->num < node->left->key->num))
		return avl_rotate_right(node);

	//Right right
	if ((balance < -1) && (key->num > node->right->key->num))
		return avl_rotate_left(node);

	//Left right
	if ((balance > 1) && (key->num > node->left->key->num))
	{
		node->left = avl_rotate_left(node->left);
		return avl_rotate_right(node);
	}

	//Right left
	if ((balance < -1) && (key->num < node->right->key->num))
	{
		node->right = avl_rotate_right(node->right);
		return avl_rotate_left(node);
	}

	return node;
}


//Returns leftmost leaf in tree
static avl_node* avl_min_node(avl_node* node)
{
	avl_node* current = node;

	while (current->left != NULL)
		current = current->left;

	return current;
}


//Deletes node with specified key from tree
static avl_node* avl_delete(avl_node* root, trie_node* key)
{
	if (root == NULL)
		return root;

	if (key->num < root->key->num)
		root->left = avl_delete(root->left, key);

	else if (key->num > root->key->num)
		root->right = avl_delete(root->right, key);

	//Key of root is the same as the key to delete
	else
	{
		//Node with at most one child.
		if ((root->left == NULL) || (root->right == NULL))
		{
			avl_node* temp = root->left ? root->left : root->right;

			//No children
			if (temp == NULL)
			{
				temp = root;
				root = NULL;
			}
			//One child
			else
			{
				*root = *temp;
			}
			free(temp);
		}
		//Node has both children
		else
		{
			avl_node* temp = avl_min_node(root->right);
			root->key = temp->key;
			root->right = avl_delete(root->right, temp->key);
		}
	}

	//Tree was empty
	if (root == NULL)
		return root;

	//Update height of current node
	root->height = max(avl_height(root->left), avl_height(root->right)) + 1;

	//Check if node is imbalanced
	int balance = avl_get_balance(root);

	//If node is imbalanced there are 4 cases:

	//Left left
	if ((balance > 1) && (avl_get_balance(root->left) >= 0))
		return avl_rotate_right(root);

	//Left right
	if ((balance > 1) && (avl_get_balance(root->left) < 0))
	{
		root->left = avl_rotate_left(root->left);
		return avl_rotate_right(root);
	}

	//Right right
	if ((balance < -1) && (avl_get_balance(root->right) <= 0))
		return avl_rotate_left(root);

	//Right left
	if ((balance < -1) && (avl_get_balance(root->right) > 0))
	{
		root->right = avl_rotate_right(root->right);
		return avl_rotate_left(root);
	}

	return root;
}


//Clears avl tree rooted in the root node
static void avl_clear(avl_node* root)
{
	if (root != NULL)
	{
		if (root->left != NULL) avl_clear(root->left);
		if (root->right != NULL) avl_clear(root->right);
		free(root);
		root = NULL;
	}
}


//end of AVL Tree Implementation
//------------------------------

//##############################
//##############################

//Trie implementation
//------------------------------

//Creates new TRIE node
static trie_node* trie_new_node(char* label, long number, long p_len, trie_node* parent)
{
	trie_node* node = malloc(sizeof(trie_node));
	node->label = str_duplicate(label);
	node->num = number;
	node->prefix_len = p_len;
	node->parent = parent;
	memset(node->children, 0, ALPHABET_SIZE * sizeof(trie_node*));
	return node;
}


//Creates new, empty TRIE with root node
static Trie* trie_create()
{
	Trie* t = malloc(sizeof(Trie));
	t->root = trie_new_node("\0", -1, 0, NULL);
	t->avl_root = NULL;
	t->nodes_amount = 1;
	t->words_amount = 0;
	return t;
}


//Returns amount of non-null children of given node
static int trie_children_count(trie_node* node)
{
	int count = 0;
	if (node != NULL)
		for (int i = 0; i < ALPHABET_SIZE; i++)
			if (node->children[i] != NULL)
				count++;
	return count;
}


//Returns pointer to node of specified number
static trie_node* trie_get_node(avl_node* node, long num)
{
	if (node == NULL)
		return NULL;
	else if (node->key->num == num)
		return node->key;
	else if (node->key->num < num)
		return trie_get_node(node->right, num);
	else
		return trie_get_node(node->left, num);
}


//Deletes specified node from trie
static void trie_delete_node(Trie** t, trie_node* node)
{
	(*t)->nodes_amount--;
	if (node->label != NULL)
		free(node->label);
	if (node != NULL)
		free(node);
	node = NULL;
}


//Inserts string into given trie
bool trie_insert(Trie** t, char* word)
{
	bool stop = false;
	long node_iter = 0;
	long word_iter = 0;
	long prefix_len = 0;

	if (t == NULL || (*t) == NULL || (*t)->nodes_amount == 0)
	{
		if (*t != NULL)
			free(*t);
		(*t) = trie_create();
	}

	trie_node* current = (*t)->root;
	trie_node* new;
	char* temp_str;
	while (!stop)
	{
		//end of word & node label
		if ((current->label[node_iter] == '\0') && (word[word_iter] == '\0'))
		{
			if (current->num == -1)
			{
				current->num = (*t)->words_amount++;
				(*t)->avl_root = avl_insert((*t)->avl_root, current);
			}
			else
			{
				return false;
			}
			stop = true;
		}
		//end of word
		else if (word[word_iter] == '\0')
		{
			prefix_len += node_iter;

			temp_str = subString(current->label, node_iter,
				strlen(current->label) - 1);
			new = trie_new_node(temp_str, current->num, prefix_len, current);
			free(temp_str);
			temp_str = subString(current->label, 0, node_iter - 1);
			free(current->label);

			if (current->num == -1)
				(*t)->avl_root = avl_insert((*t)->avl_root, current);
			current->label = temp_str;
			current->num = (*t)->words_amount++;

			if (new->num != -1)
				(*t)->avl_root = avl_insert((*t)->avl_root, new);


			for (int i = 0; i < ALPHABET_SIZE; i++)
			{
				new->children[i] = current->children[i];
				if (new->children[i] != NULL)
				{
					new->children[i]->parent = new;
				}
				current->children[i] = NULL;
			}
			current->children[new->label[0] - 'a'] = new;
			(*t)->nodes_amount++;

			stop = true;
		}
		//end of node label
		else if (current->label[node_iter] == '\0')
		{
			prefix_len += node_iter;
			//further child already exists
			if (current->children[word[word_iter] - 'a'] != NULL)
			{
				node_iter = 0;
				current = current->children[word[word_iter] - 'a'];
			}
			//there is no child with adequate first letter
			else
			{
				temp_str = subString(word, word_iter, strlen(word) - 1);
				new = trie_new_node(temp_str, (*t)->words_amount++,
					prefix_len, current);
				current->children[word[word_iter] - 'a'] = new;
				(*t)->avl_root = avl_insert((*t)->avl_root, new);
				(*t)->nodes_amount++;
				free(temp_str);
				stop = true;
			}
		}
		//character conflict
		else if (word[word_iter] != current->label[node_iter])
		{
			prefix_len += node_iter;

			temp_str = subString(word, word_iter, strlen(word) - 1);
			new = trie_new_node(temp_str, (*t)->words_amount++, prefix_len, current);
			free(temp_str);

			temp_str = subString(current->label, node_iter,
				strlen(current->label) - 1);
			trie_node* new2;
			new2 = trie_new_node(temp_str, current->num, prefix_len, current);
			free(temp_str);

			(*t)->avl_root = avl_delete((*t)->avl_root, current);

			temp_str = subString(current->label, 0, node_iter - 1);
			free(current->label);
			current->label = temp_str;
			current->num = -1;

			for (int i = 0; i < ALPHABET_SIZE; i++)
			{
				new2->children[i] = current->children[i];
				if (new2->children[i] != NULL)
					new2->children[i]->parent = new2;
				current->children[i] = NULL;
			}
			current->children[new->label[0] - 'a'] = new;
			current->children[new2->label[0] - 'a'] = new2;

			(*t)->avl_root = avl_insert((*t)->avl_root, new);
			if (new2->num != -1)
				(*t)->avl_root = avl_insert((*t)->avl_root, new2);
			(*t)->nodes_amount += 2;
			stop = true;
		}
		else
		{
			word_iter++;
			node_iter++;
		}
	}
	return true;
}


//Returns true if trie contains specified string, false otherwise
bool trie_find(Trie* t, char* word)
{
	if (t == NULL)
		return false;

	trie_node* current = t->root;
	if (strlen(word) == 0 || t->root == NULL)
		return false;
	current = current->children[word[0] - 'a'];

	bool stop = false;
	long node_iter = 0;
	long node_len = 0;
	long word_iter = 0;
	long word_len = strlen(word);

	if (current != NULL)
		node_len = strlen(current->label);

	while (!stop && word_iter < word_len)
	{
		if (current == NULL)
		{
			stop = true;
		}
		else if (current->label[node_iter] != word[word_iter])
		{
			stop = true;
		}
		else if (current->label[node_iter] == word[word_iter])
		{
			word_iter++;
			node_iter++;
			if ((node_iter == node_len) && (word_iter < word_len))
			{
				current = current->children[word[word_iter] - 'a'];
				node_iter = 0;
				if (current != NULL)
					node_len = strlen(current->label);
			}
		}
	}

	return (!stop);
}


//Inserts prefix of given word, including both indexes, into a trie
bool trie_prev(Trie** t, long num, long from, long to)
{
	if (t == NULL || *t == NULL)
		return false;

	trie_node* node = trie_get_node((*t)->avl_root, num);

	if (node == NULL)
		return false;

	long found_len = node->prefix_len + strlen(node->label) - 1;
	long word_len = to - from + 1;
	long current_len = strlen(node->label) - 1;

	if (to > found_len)
		return false;
	else
	{
		char* word = malloc(sizeof(char) * (word_len + 1));
		word[word_len] = '\0';
		while (found_len >= from)
		{
			if (current_len < 0 && found_len >= from)
			{
				node = node->parent;
				current_len = strlen(node->label) - 1;
			}
			if (found_len <= to)
			{
				word[word_len - 1] = node->label[current_len];
				word_len--;
			}
			current_len--;
			found_len--;
		}
		if (!trie_insert(t, word))
		{
			free(word);
			return false;
		}
		free(word);
	}
	return true;
}


//Removes node with specified number from the trie
bool trie_delete(Trie** t, long num)
{
	trie_node* node = trie_get_node((*t)->avl_root, num);
	if (node == NULL)
		return false;

	trie_node* parent = node->parent;

	(*t)->avl_root = avl_delete((*t)->avl_root, node);

	if (trie_children_count(node) == 0)
	{
		parent->children[node->label[0] - 'a'] = NULL;
		trie_delete_node(t, node);

		//Merge nodes        
		if ((parent != (*t)->root) &&
			(trie_children_count(parent) == 1) && (parent->num == -1))
		{
			trie_node* child = NULL;
			int iter = 0;
			while (child == NULL)
				child = parent->children[iter++];

			(*t)->avl_root = avl_delete((*t)->avl_root, child);

			long label_size = strlen(child->label) + strlen(parent->label) + 1;
			char* new_label = malloc(sizeof(char) * (label_size));
			memset(new_label, '\0', label_size);

			strcat(new_label, parent->label);
			strcat(new_label, child->label);
			parent->num = child->num;
			parent->children[iter - 1] = NULL;

			trie_delete_node(t, child);

			free(parent->label);
			parent->label = new_label;
			(*t)->avl_root = avl_insert((*t)->avl_root, parent);
		}
	}
	else
	{
		node->num = -1;
	}
	return true;
}


//Removes tree rooted in specified node
static void trie_clear_subtree(Trie** t, trie_node* node)
{
	if (node != NULL)
	{
		for (int i = 0; i < ALPHABET_SIZE; i++)
			trie_clear_subtree(t, node->children[i]);
		trie_delete_node(t, node);
	}
}


//Removes trie
void trie_clear(Trie** t)
{
	if ((t != NULL) && (*t != NULL))
	{
		avl_clear((*t)->avl_root);
		trie_clear_subtree(t, (*t)->root);
	}
	(*t)->root = NULL;
	(*t)->avl_root = NULL;
}

//end of Trie implementation
//------------------------------
